from django.apps import AppConfig


class SettingsPortfolioConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'settings_portfolio'
    verbose_name = "Portafolio"
